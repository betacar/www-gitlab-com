<!-- See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process -->

### What is this blog post about?

<!-- After you commit a version of your blog post, update the link below to point to the blog post review app -->
**Review App**: https://XXXXX.about.gitlab.com/YYYY/MM/DD/your-blog-post/

<!-- What do you want to blog about? Add your description here -->

### What are the relevant issue numbers?

<!-- All blog posts should have a corresponding issue. Create an issue now if needed and add the link below -->
Closes https://gitlab.com/gitlab-com/www-gitlab-com/issues/XXXX

### Checklist

- [ ] Link to issue added, and set to close the when this MR is merged
- [ ] Due date and milestone added for the desired publish date
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#time-sensitive-posts-official-announcements-company-updates-breaking-changes-and-news)
  - [ ] Added ~"priority" label
  - [ ] Mentioned `@rebecca` to give her a heads up ASAP
- [ ] Link to the Review App added above
- [ ] Reviewed by team member
- [ ] Reviewed by [content team](https://about.gitlab.com/handbook/marketing/blog/#editorial-reviews)

/label ~"blog post"
